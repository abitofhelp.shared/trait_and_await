use async_trait::async_trait;

use crate::error::ApiError;

#[async_trait]
pub trait ValidateCommandOrQuery {
    fn validate() -> Option<ApiError>;
}
