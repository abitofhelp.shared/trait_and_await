use crate::project::Project;
use crate::project_http::ProjectHttp;

pub struct ProjectHttpMapper {}

impl ProjectHttpMapper {
    pub fn to_model(rest: ProjectHttp) -> Project {
        Project::new(rest.name.to_string(), rest.size, rest.timezone)
    }

    pub fn to_http(model: Project) -> ProjectHttp {
        ProjectHttp::new(model.name.to_string(), model.size, model.timezone)
    }
}
