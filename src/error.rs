use std::error::Error;

use axum::{http::StatusCode, response::IntoResponse, Json};
use serde_json::json;
use thiserror::Error;

pub type BoxError = Box<dyn Error + Send + Sync + 'static>;

#[derive(Error, Debug)]
#[non_exhaustive]
pub enum ApiError {
    #[error("application error(code=500): {message}")]
    Application {
        message: String,
        err: Option<BoxError>,
    },

    #[error("request is malformed(code=400): {message}")]
    BadRequest {
        message: String,
        err: Option<BoxError>,
    },

    #[error("request conflicts with an existing resource(code=409): {message}")]
    Conflict {
        message: String,
        err: Option<BoxError>,
    },

    #[error("resource not allowed(code=403): {message}")]
    Forbidden {
        message: String,
        err: Option<BoxError>,
    },

    #[error("an internal server error occurred(code=500): {message}")]
    InternalServerError {
        message: String,
        err: Option<BoxError>,
    },

    #[error("requested resource was not found(code=404): {message}")]
    NotFound {
        message: String,
        err: Option<BoxError>,
    },

    #[error("not authenticated or token expired(code=401): {message}")]
    Unauthorized {
        message: String,
        err: Option<BoxError>,
    },
}

impl IntoResponse for ApiError {
    fn into_response(self) -> axum::response::Response {
        let (status, err_msg) = match self {
            Self::InternalServerError { message, err } => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Message: {}, Error: {:?}", message, err),
            ),
            Self::Application { message, err } => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Message: {}, Error: {:?}", message, err),
            ),
            Self::BadRequest { message, err } => (
                StatusCode::BAD_REQUEST,
                format!("Message: {}, Error: {:?}", message, err),
            ),
            Self::Conflict { message, err } => (
                StatusCode::CONFLICT,
                format!("Message: {}, Error: {:?}", message, err),
            ),
            Self::Forbidden { message, err } => (
                StatusCode::FORBIDDEN,
                format!("Message: {}, Error: {:?}", message, err),
            ),
            Self::NotFound { message, err } => (
                StatusCode::NOT_FOUND,
                format!("Message: {}, Error: {:?}", message, err),
            ),
            Self::Unauthorized { message, err } => (
                StatusCode::UNAUTHORIZED,
                format!("Message: {}, Error: {:?}", message, err),
            ),
        };
        (status, Json(json!({ "error": err_msg }))).into_response()
    }
}
