use async_trait::async_trait;

use crate::error::ApiError;
use crate::find_one_project_query::FindOneProjectQuery;
use crate::find_one_project_query_port::FindOneProjectQueryPort;
use crate::find_one_project_usecase::FindOneProjectUseCase;
use crate::project::Project;

#[derive(Clone)]
pub struct FindOneProjectService {
    query_port: Box<dyn FindOneProjectQueryPort>,
}

impl FindOneProjectService {
    pub fn new(query_port: Box<dyn FindOneProjectQueryPort + Send + Sync>) -> Self {
        Self { query_port }
    }
}

#[async_trait]
impl FindOneProjectUseCase for FindOneProjectService {
    async fn find_one(&self, query: FindOneProjectQuery) -> Result<Project, ApiError> {
        self.query_port.find_one(query.name).await
    }
}
