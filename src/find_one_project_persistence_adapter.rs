use async_trait::async_trait;

use crate::app_id::AppId;
use crate::error::ApiError;
use crate::find_one_project_query_port::FindOneProjectQueryPort;
use crate::find_one_project_repository::FindOneProjectRepository;
use crate::project::Project;
use crate::project_entity_mapper::ProjectEntityMapper;

#[derive(Clone)]
pub struct FindOneProjectPersistenceAdapter {
    repository: FindOneProjectRepository,
}

impl FindOneProjectPersistenceAdapter {
    pub const fn new(repository: FindOneProjectRepository) -> Self {
        Self { repository }
    }
}

#[async_trait]
impl FindOneProjectQueryPort for FindOneProjectPersistenceAdapter {
    async fn find_one(&self, name: AppId) -> Result<Project, ApiError> {
        match self.repository.find_one(name.to_string()) {
            Ok(pe) => Ok(ProjectEntityMapper::to_model(pe)),
            Err(e) => Err(e),
        }
        //.await
        //.map_or_else(|e| Err(e), |pe| Ok(ProjectEntityMapper::to_model(pe)))
        // {
        //     Ok(pe) => |pe| Ok(ProjectEntityMapper::to_model(pe))
        //     Err(e: ApiError) => Err(e as ApiError)
        // }
    }
}
