use std::future::Future;

use axum::extract::{Path, State};
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::Json;
use axum_macros::debug_handler;

use crate::find_one_project_query::FindOneProjectQuery;
use crate::find_one_project_service::FindOneProjectService;
use crate::find_one_project_usecase::FindOneProjectUseCase;
use crate::project_http_mapper::ProjectHttpMapper;

mod app_id;
mod database_connection;
mod error;
mod find_one_project_persistence_adapter;
mod find_one_project_query;
mod find_one_project_query_port;
mod find_one_project_repository;
mod find_one_project_service;
mod find_one_project_usecase;
mod inmemory_database_connection;
mod project;
mod project_entity;
mod project_entity_mapper;
mod project_http;
mod project_http_mapper;
mod validate_command_or_query;

struct Concrete__Adapter {}

trait SlowDrying {
    fn hand_print(&self) -> &'static str;
}

impl SlowDrying for Concrete__Adapter {
    fn hand_print(&self) -> &'static str {
        "🖐️"
    }
}

#[debug_handler]
async fn find_one_handler(
    Path(name): Path<String>,
    State(state): State<SharedState>,
) -> impl IntoResponse {
    //Result<Json<ProjectHttp>, (StatusCode, ApiError)> {

    let find_one_project_query = FindOneProjectQuery::from(name.as_str());

    state
        .app_state
        .service
        .find_one(find_one_project_query)
        .await
        .map_or_else(
            |e| (StatusCode::NOT_FOUND, e).into_response(),
            |p| (StatusCode::OK, Json(ProjectHttpMapper::to_http(p))).into_response(),
        )
}

#[cfg(test)]
mod tests {
    use axum::routing::get;
    use axum::Router;

    use crate::find_one_project_persistence_adapter::FindOneProjectPersistenceAdapter;
    use crate::find_one_project_query_port::FindOneProjectQueryPort;
    use crate::find_one_project_repository::FindOneProjectRepository;
    use crate::find_one_project_service::FindOneProjectService;
    use crate::inmemory_database_connection::new_with_sample_data;

    use super::*;

    #[tokio::test]
    async fn test() {
        let database_connection = new_with_sample_data().unwrap();
        let find_one_project_repository =
            FindOneProjectRepository::new_from_connection(database_connection).unwrap();
        let find_one_project_persistence_adapter =
            FindOneProjectPersistenceAdapter::new(find_one_project_repository);
        let pa: Box<dyn FindOneProjectQueryPort + Send + Sync> =
            Box::new(find_one_project_persistence_adapter);

        let find_one_project_service = Box::new(FindOneProjectService::new(pa));
        let state = SharedState {
            app_state: AppState {
                service: find_one_project_service,
            },
        };

        let router = Router::new()
            .route("/project/:name", get(find_one_handler))
            .with_state(state);

        let server = axum::Server::bind(&std::net::SocketAddr::from(([0, 0, 0, 0], 3000)))
            .serve(router.into_make_service());

        if let Err(err) = server.await {
            eprintln!("server error: {}", err);
        }
    }
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    Ok(())
}

#[derive(Clone)]
pub struct AppState {
    pub service: Box<FindOneProjectService>,
}

#[derive(Clone)]
pub struct SharedState {
    pub app_state: AppState,
}
