use std::sync::Arc;

use parking_lot::RwLock;

use crate::database_connection::DatabaseConnection;
use crate::error::ApiError;
use crate::project_entity::ProjectEntity;

pub type InMemoryDatabase = RwLock<Vec<ProjectEntity>>;

pub type InMemoryDatabaseConnection = Arc<DatabaseConnection<InMemoryDatabase>>;

pub fn new() -> Result<InMemoryDatabaseConnection, ApiError> {
    let inmemory_database = RwLock::new(vec![]);

    Ok(InMemoryDatabaseConnection::from(DatabaseConnection {
        database: inmemory_database,
    }))
}

pub fn new_with_sample_data() -> Result<InMemoryDatabaseConnection, ApiError> {
    let inmemory_database = RwLock::new(vec![
        ProjectEntity::new(None, "abc".to_string(), 123, "America/Phoenix".to_string()).unwrap(),
        ProjectEntity::new(
            None,
            "xyz".to_string(),
            321,
            "America/Los_Angeles".to_string(),
        )
        .unwrap(),
    ]);

    Ok(InMemoryDatabaseConnection::from(DatabaseConnection {
        database: inmemory_database,
    }))
}
