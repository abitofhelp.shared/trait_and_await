//! This file contains a trait enforcing the requirements of a query port for finding a single
//! project in an external persistence system.

use async_trait::async_trait;
use dyn_clone::DynClone;

use crate::app_id::AppId;
use crate::error::ApiError;
use crate::project::Project;

dyn_clone::clone_trait_object!(FindOneProjectQueryPort);

/// This is a trait providing a method to find a single project through an egress/driven command port
/// in an external persistence system.
#[async_trait]
pub trait FindOneProjectQueryPort: DynClone + Send + Sync {
    async fn find_one(&self, name: AppId) -> Result<Project, ApiError>;
}
