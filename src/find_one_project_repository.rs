use crate::error::ApiError;
use crate::inmemory_database_connection::InMemoryDatabaseConnection;
use crate::project_entity::ProjectEntity;

#[derive(Clone)]
pub struct FindOneProjectRepository {
    connection: InMemoryDatabaseConnection,
}

impl FindOneProjectRepository {
    pub fn new_from_connection(connection: InMemoryDatabaseConnection) -> Result<Self, ApiError> {
        // With a more complex database, it is likely that there would be errors, which would
        // need to return a Result<>.  The inmemory database only has a success condition.
        Ok(Self { connection })
    }

    pub fn find_one(&self, name: String) -> Result<ProjectEntity, ApiError> {
        self.connection
            .database
            .read()
            .iter()
            .find(|pe| pe.name == name)
            .map_or_else(
                || {
                    Err(ApiError::NotFound {
                        message: format!("a project named '{name}' does not exist"),
                        err: None,
                    })
                },
                |pe| Ok(pe.clone()),
            )
    }
}
