use serde::{Deserialize, Serialize};

// Represents a list of projects
pub type ProjectList = Vec<ProjectHttp>;

// Represents a project
#[derive(Clone, Debug, Deserialize, Serialize)]
#[non_exhaustive]
pub struct ProjectHttp {
    pub name: String,
    pub size: usize,
    pub timezone: String,
}

impl ProjectHttp {
    #[inline]
    pub fn new(name: String, size: usize, timezone: String) -> Self {
        Self {
            name,
            size,
            timezone,
        }
    }

    #[inline]
    pub fn from(name: &str, size: usize, timezone: &str) -> Self {
        ProjectHttp::new(name.to_owned(), size, timezone.to_owned())
    }
}
